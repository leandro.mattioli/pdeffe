pub enum UnderlineSpec {
    None,
    Simple,
    Double
}

pub struct CharacterStyle {
    //None means --> inherit
    character_style: Option<bool>,
    italic: Option<bool>,
    underline: Option<UnderlineSpec>,
    font_size: Option<u8>,
}