use crate::InlineContent::InlineContent;
use crate::ParagraphStyle::ParagraphStyle;

pub struct Paragraph {
    content: Vec<InlineContent>,
    style: ParagraphStyle,
    width: u8,
    height: u8
}