use printpdf::*;

pub enum Alignment {
    Left,
    Center,
    Right,
    Justified {justify_breaks: bool}
}

pub enum WordWrapPolicy {
    NoWrap,
    WordWrap,
    WrapAnywhere
}

pub struct ParagraphStyle {
    font: IndirectFontRef,
    font_size: u8,
    leading: u8,
    left_indent: u8,
    right_indent: u8,
    first_line_indent: u8,
    alignment: Alignment,
    space_before: u8,
    space_after: u8,
    word_wrap: WordWrapPolicy,
    justify_last_line: bool,
    border_width: u8,
    border_padding: u8,
    border_color: Color,
    border_radius: u8,
    split_long_words: u8,
    text_color: Color,
    back_color: Color
}

impl Default for ParagraphStyle {
    fn default() -> ParagraphStyle {
        ParagraphStyle {
            font: (),
            font_size: 12,
            leading: 16,
            left_indent: 0,
            right_indent: 0,
            first_line_indent: 0,
            alignment: Alignment::Left,
            space_before: 0,
            space_after: 0,
            word_wrap: WordWrapPolicy::WordWrap,
            justify_last_line: false,
            border_width: 0,
            border_padding: 0,
            border_color: (),
            border_radius: 0,
            split_long_words: 0,
            text_color: (),
            back_color: (),
        }
    }
}
