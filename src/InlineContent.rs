use printpdf::*;
use crate::CharacterStyle::CharacterStyle;

pub enum InlineContent {
    TextFragment {text: String, style: CharacterStyle, hyperlink: Option<String>},
    ManualLineBreak,
    InlineMath {formula: String},
    InlineImage {image: ImageXObject},
    Quantity{value: String, unit_spec: String}, //<qty unit="\meters" value="10" />
}