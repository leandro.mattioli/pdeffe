mod Paragraph;
mod ParagraphStyle;
mod InlineContent;
mod CharacterStyle;

use printpdf::*;
use std::fs::File;
use std::io::BufWriter;

//https://stackoverflow.com/questions/75658182/unsure-how-to-format-my-pdf-with-the-printpdf-0-5-3-crate-in-rust


pub fn add(left: usize, right: usize) -> usize {
    left + right
}

pub fn test_pdf() {
    let (doc, page1, layer1) = PdfDocument::new("PDF_Document_title", Mm(297.0), Mm(210.0), "Layer 1");
    let (page2, layer1) = doc.add_page(Mm(10.0), Mm(250.0),"Page 2, Layer 1");

    doc.save(&mut BufWriter::new(File::create("test_working.pdf").unwrap())).unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
        test_pdf();
    }
}
